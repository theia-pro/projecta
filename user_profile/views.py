from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from user_profile.models import Profile
from django.contrib.auth.models import User

@login_required()
def profile(request):
    # postid = request.POST["id"]
    context = {
        'User': request.user.username,
        'given_likes': Profile.objects.get(user=str(request.user.id)).given_likes
    }
    # return redirect('/')
    return render(request, 'user_profile/user_profile.html', context=context)

    # return render(request, "delete/delete.html")