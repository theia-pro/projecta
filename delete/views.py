from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from newpost.models import Post


@login_required()
def delete(request):
    postid = request.POST["id"]
    Post.objects.filter(id=postid).delete()
    return redirect('/')
    # return render(request, 'delete/delete.html', context=context)

    # return render(request, "delete/delete.html")
