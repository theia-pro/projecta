from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.shortcuts import render
import requests
import json
from newpost.models import Post
import django.dispatch


def index(request):
    if request.user.id:
        author = request.user
        context = {
            'Posts': Post.objects.all(),
            'Author': author
        }
        return render(request, 'index_a.html', context=context)
    else:
        data = requests.get("http://python-django-269309.appspot.com/api/v2/images/?format=json")

        context = {
            'response': data,
        }
        return render(request, 'index_b.html', context=context)
        # return redirect('../accounts/login/')


