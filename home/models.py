from django.db import models

from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.api import APIField
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.images.formats import Format, register_image_format

class HomePage(Page):
    templates = "home/home_page.html"
    main_title = models.CharField(max_length=200, blank=True, null=True)
    body = RichTextField()
    main_image = models.ImageField(null=True,blank=True)
    content_panels = Page.content_panels + [
        FieldPanel("main_title"), FieldPanel("body"), FieldPanel("main_image"),
    ]
    api_fields = [
        APIField('main_title'),
        APIField('body'),
        APIField('main_image'),
    ]

