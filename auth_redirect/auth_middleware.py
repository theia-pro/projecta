from django.core.exceptions import PermissionDenied
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.http import HttpRequest
import geoip2.database
from django.contrib.gis.geoip2 import GeoIP2
from pprint import pprint
import requests
from auth_redirect.models import SecurityData


g = GeoIP2()

class AuthRequiredMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        ip = requests.get('https://api.ipify.org').text
        result = g.country(ip)
        response = self.get_response(request)
        location = result['country_code']
        if location != "DK":
            create_record = SecurityData(request_location=location, ip=ip)
            create_record.save()
        else:
            pass

        return response
