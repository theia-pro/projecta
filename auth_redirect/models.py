from django.db import models
from django.utils.timezone import now

# Create your models here.
class SecurityData(models.Model):
    request_location = models.CharField(
        default=None,
        max_length=20
    )
    ip = models.CharField(
        default=None,
        max_length=20
    )
    created_date = models.DateTimeField(default=now, editable=False)

    def __str__(self):
        return self.request_location

