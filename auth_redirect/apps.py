from django.apps import AppConfig


class AuthRedirectConfig(AppConfig):
    name = 'auth_redirect'
