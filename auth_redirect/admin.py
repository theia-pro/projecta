from django.contrib import admin
from .models import SecurityData

admin.site.site_header = 'Python-Django'

class SecurityDataAdmin(admin.ModelAdmin):
    list_display = ('request_location', 'created_date', 'ip')
    # list_filter = ('due', 'status', 'publication_type',)


admin.site.register(SecurityData, SecurityDataAdmin)


