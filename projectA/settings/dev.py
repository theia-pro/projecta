from .base import *
import os

if os.getenv('GAE_APPLICATION', None):
    # SECURITY WARNING: don't run with debug turned on in production!
    # SECURITY WARNING: define the correct hosts in production!
    DEBUG = False
    ALLOWED_HOSTS = ['python-django-269309.appspot.com']
    SECRET_KEY = os.environ['SECRET_KEY']
else:
    DEBUG = True
    ALLOWED_HOSTS = ['python-django-269309.appspot.com', '127.0.0.1', 'localhost']
    SECRET_KEY = 'agqst&=fq8txq1xpqe7frv(cxzcm)d!cnj3f(bd%4+i^k)=9!6'


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# Email Settings



try:
    from .local import *
except ImportError:
    pass

