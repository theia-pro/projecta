from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.urls import path
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls
from django.views.generic import RedirectView
from home import views
from .api import api_router
from register import views as v
from newpost import views as p
from delete import views as delete
from like import views as like
from delete_user import views as delete_user
from user_profile import views as profile
from django.conf import settings
from django.conf.urls.static import static

from search import views as search_views

urlpatterns = [
    path('', views.index, name='index'),

    url(r'^django-admin/', admin.site.urls),

    url(r'^admin/', include(wagtailadmin_urls)),
    url(r'^documents/', include(wagtaildocs_urls)),
    url(r'^search/$', search_views.search, name='search'),
    path('accounts/', include('django.contrib.auth.urls')),
    url(r'^register/', v.register, name="register"),
    url(r'^create-post/', p.newpost, name="newpost"),
    url(r'^delete/', delete.delete, name="delete"),
    url(r'^like/', like.like, name="like"),
    url(r'^profile/', profile.profile, name="profile"),
    url(r'^delete-profile/', delete_user.delete_user, name="delete_user"),
    # The next two lines must be at the bottom and next to each other
    url(r'^api/v2/', api_router.urls),
    url(r'', include(wagtail_urls)),

]


if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns = urlpatterns + [
    # For anything not caught by a more specific rule above, hand over to
    # Wagtail's page serving mechanism. This should be the last pattern in
    # the list:
    url(r"", include(wagtail_urls)),

    # Alternatively, if you want Wagtail pages to be served from a subpath
    # of your site, rather than the site root:
    #    url(r"^pages/", include(wagtail_urls)),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)