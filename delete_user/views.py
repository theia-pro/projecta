from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.contrib import auth
from newpost.models import Post

def delete_user(request):
    userid = request.user.id
    user = User.objects.filter(id=userid)
    user.delete()
    auth.logout(request)
    return redirect('/')
    # return render(request, 'delete_user/delete_user.html', context=context)

