from django.urls import path
from . import views

urlpatterns = [
    path('', views.delete_user, name='delete_user'),
]