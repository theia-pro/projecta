from django.contrib import admin
from .models import Post

class PostDataAdmin(admin.ModelAdmin):
    list_display = ('id', 'header', 'author', 'time_stamp', 'textarea', 'post_image', 'likes')
    # list_filter = ('due', 'status', 'publication_type',)


admin.site.register(Post, PostDataAdmin)
