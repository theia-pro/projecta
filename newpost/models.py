from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Post(models.Model):
    header = models.CharField(max_length=130)
    author = models.CharField(max_length=130, null=True)
    time_stamp = models.DateTimeField(auto_now_add=True)
    textarea = models.CharField(max_length=2000, blank=True)
    post_image = models.ImageField(null=True, blank=True)
    likes = models.IntegerField(default=0)

