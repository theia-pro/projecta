from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView
from .models import Post
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.shortcuts import redirect

# Create your views here.
@login_required
def newpost(request):
    if request.method == "POST":
        header = request.POST["header"]
        textarea = request.POST["textarea"]
        author = request.user
        uploaded_file = request.FILES['post-image']
        fs = FileSystemStorage()
        fs.save(uploaded_file.name, uploaded_file)
        createpost = Post(header=header, textarea=textarea, author=author, post_image=uploaded_file.name)
        createpost.save()
        return redirect('/')
    return render(request, "newpost/newpost.html")