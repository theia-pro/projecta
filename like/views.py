from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import redirect, render
from newpost.models import Post
from user_profile.models import Profile


@login_required()
def like(request):
    postid = request.POST["id"]
    # START Here the post gets the like START
    currentlikes = Post.objects.get(id=postid).likes
    newLike = currentlikes + 1
    Post.objects.filter(id=postid).update(likes=newLike)
    finalLikes = Post.objects.get(id=postid).likes
    # End
    # START Here the user profile gets the like
    userid = request.user
    givenLikes = Profile.objects.get(user=userid).given_likes
    newGivenLike = givenLikes + 1
    Profile.objects.filter(user=userid).update(given_likes=newGivenLike)
    # End
    context = {
        'id': postid,
        'likes': currentlikes,
        'finalLikes': finalLikes

    }
    return redirect('/')
    # return render(request, 'like/like.html', context=context)

    # return render(request, "delete/delete.html")
