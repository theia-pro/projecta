from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from .forms import RegistrationForm
from django.core.mail import send_mail


# Create your views here.
def register(response):
    if response.method == "POST":
        form = RegistrationForm(response.POST)
        if form.is_valid():
            form.save()
            new_user = authenticate(username=form.cleaned_data['username'],
                                    password=form.cleaned_data['password1'],
                                    )
            login(response, new_user)
            send_mail('Welcome to Metropol', 'Are you ready to travel? Go to our site now!', 'email@app.theia.pro', [form.cleaned_data['email']])
            return HttpResponseRedirect("/")
    else:
        form = RegistrationForm()

    return render(response, "register/register.html", {"form": form})
